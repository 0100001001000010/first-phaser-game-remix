/*global Phaser game game_state, dialogTextLeft, dialogTextRight i _this loadText*/
game_state.bosslevel = function(){};
game_state.bosslevel.prototype = {
    preload: function(){
        game.load.spritesheet('player', 'assets/img/stickman.png', 26, 76);
        game.load.spritesheet('MSCEO', 'assets/img/satyanadella.png', 400, 452);
        game.load.spritesheet('virus', 'assets/img/virus.png', 37, 37);
        game.load.spritesheet('tux', 'assets/img/tux.png', 100, 100);
        game.load.spritesheet('platformBlock', 'assets/img/hexdigits.png', 10, 14);
    },
    
    create: function(){
        //Generating the ledges. This code is mostly reused from mainGame.js.
        console.log("Generating the ledges...");
			this.ledges     = game.add.group();
			for (i[0] = 0; i[0] < 600; i[0] += 14){                             //Left
				this.ledges.create(0, i[0], 'platformBlock');
				this.ledges.getAt(this.ledges.length - 1).frame = Math.floor(Math.random() * 16);
			} for (i[0] = 0; i[0] < 800; i[0] += 10){                           //Top
				this.ledges.create(i[0], 0, 'platformBlock');
				this.ledges.getAt(this.ledges.length - 1).frame = Math.floor(Math.random() * 16);
			} for (i[0] = 0; i[0] < 600; i[0] += 14){                           //Right
				this.ledges.create(790, i[0], 'platformBlock');
				this.ledges.getAt(this.ledges.length - 1).frame = Math.floor(Math.random() * 16);
			} for (i[0] = 0; i[0] < 800; i[0] += 10){                           //Bottom
				this.ledges.create(i[0], 586, 'platformBlock');
				this.ledges.getAt(this.ledges.length - 1).frame = Math.floor(Math.random() * 16);
			}
			
		//Generate the characters and start the physics. Mostly reused from mainGame.js.
		this.MSCEO = game.add.sprite(200, 113, "MSCEO");
        this.MSCEO.frame = 0;
        this.player = game.add.sprite(10, 510, 'player');
        this.player.frame = 5;
        this.player.animations.add("left", [0, 1, 2, 3, 4], 10, true);
        this.player.animations.add("right",[6, 7, 8, 9, 10],10, true);
        this.viruses = game.add.group();
        game.physics.startSystem(Phaser.Physics.ARCADE);
		game.physics.arcade.enable(this.ledges);
		game.physics.arcade.enable(this.viruses);
		game.physics.arcade.enable(this.player);
		game.physics.arcade.enable(this.MSCEO);
		this.viruses.enableBody = true;
		this.player.enableBody = true;
		this.ledges.enableBody = true;
		this.MSCEO.enableBody = true;
		this.ledges.forEach(function(item){
			item.body.immovable = true;
		});
		this.MSCEO.body.immovable = true;
		this.cursors = game.input.keyboard.createCursorKeys();
		this.player.body.gravity.y = 980;
		this.player.body.bounce.y = 0;
		this.keys = {
			WASD: {
				up:    game.input.keyboard.addKey(Phaser.Keyboard.W),
				left:  game.input.keyboard.addKey(Phaser.Keyboard.A),
				down:  game.input.keyboard.addKey(Phaser.Keyboard.S),
				right: game.input.keyboard.addKey(Phaser.Keyboard.D)
			}
		};
			
		this.player.health= 25;
		this.MSCEO.health = 100;
	    _this = this;
	    this.virusTimer = game.time.create(false);
	    this.virusTimer.loop(1000, function(){
	    	_this.viruses.create(Math.floor(100 + Math.random() * 600), Math.floor(100 + Math.random() * 400), "virus");
	    	_this.viruses.getAt(this.viruses.length - 1).animations.add("spin", [0, 1], 10, true);
	    	_this.viruses.getAt(this.viruses.length - 1).animations.play("spin");
	    }, this);
	    this.virusTimer.start();
        dialogTextLeft = game.add.text(16, 16, "", {font: '16px monospace', fill:'#0f0'});
        dialogTextRight = game.add.text(416, 16, "", {font: '16px monospace', fill:'#0f0'});
    },
    
    update: function(){
        dialogTextLeft.text = "Player Health: " + this.player.health;
        dialogTextRight.text= "Boss Health: " + this.MSCEO.health;
    		if (!this.player.alive){
    			game.state.start('crashed-Microsoft');
    		} if (!this.MSCEO.alive){
    			game.state.start("winning");
    		}
    		game.physics.arcade.collide(this.player, this.ledges);
	        game.physics.arcade.collide(this.tux, this.ledges, this.destroyTux);
    		
    		//Manage the player
	        if (this.cursors.left.isDown){
    	        this.player.body.velocity.x = -500;
        	    this.player.animations.play('left');
	        } else if (this.cursors.right.isDown){
    	        this.player.body.velocity.x = 500;
        	    this.player.animations.play('right');
	        } else {
    	        this.player.body.velocity.x = 0;
        	    this.player.animations.stop();
            	this.player.frame = 5;
	        }
    	    if (this.cursors.up.isDown && this.player.body.touching.down) {
        	    this.player.body.velocity.y = -1000;
	        }
	        
	        //Viruses
	        this.viruses.forEachAlive(function(item){
	        	if (item.x < _this.player.x){                                   //If the virus is to the left of the player
	        		item.body.velocity.x = 50;
	        	} if (item.x > _this.player.x) {                                //If it is to the right of the player
	        		item.body.velocity.x = -50;
	        	} if (item.y < _this.player.y){                                 //If it is above the player
	        		item.body.velocity.y = 50;
	        	} if (item.y > _this.player.y){                                 //If below
	        	    item.body.velocity.y = -50;
	        	}
	        	game.physics.arcade.collide(item, _this.tux, function(){
	        		_this.destroyTux();
	        		_this.MSCEO.damage(1);
	        		item.kill();
	        	});
	        	game.physics.arcade.collide(item, _this.player, function(){
	        		_this.player.damage(1);
	        		item.kill();
	        	});
	        });
	        
	        //Firing the bullet
	        if (this.shootingTux){
	        	if (this.tux.frame === 0) {
	        		this.tux.body.velocity.y = -2000;
	        	} else if (this.tux.frame == 1) {
	        		this.tux.body.velocity.x = 2000;
	        	} else if (this.tux.frame == 2) {
	        		this.tux.body.velocity.y = 2000;
	        	} else {
	        		this.tux.body.velocity.x = -2000;
	        	}
	        } else {
	        	if (this.keys.WASD.right.isDown) {
	        		this.tux = game.add.sprite(this.player.x, this.player.y, 'tux');
	        		this.tux.frame = 1;
	        		this.tux.enableBody = true;
	        		game.physics.arcade.enable(this.tux);
	        		this.tux.scale.setTo(0.25, 0.25);
	        		this.shootingTux = true;
	        	} else if (this.keys.WASD.left.isDown) {
	        		this.tux = game.add.sprite(this.player.x, this.player.y, 'tux');
	        		this.tux.frame = 3;
	        		this.tux.enableBody = true;
	        		game.physics.arcade.enable(this.tux);
	        		this.tux.scale.setTo(0.25, 0.25);
	        		this.shootingTux = true;
	        	} else if (this.keys.WASD.up.isDown) {
	        		this.tux = game.add.sprite(this.player.x, this.player.y, 'tux');
	        		this.tux.frame = 0;
	        		this.tux.enableBody = true;
	        		game.physics.arcade.enable(this.tux);
	        		this.tux.scale.setTo(0.25, 0.25);
	        		this.shootingTux = true;
	        	} else if (this.keys.WASD.down.isDown) {
	        		this.tux = game.add.sprite(this.player.x, this.player.y, 'tux');
	        		this.tux.frame = 2;
	        		this.tux.enableBody = true;
	        		game.physics.arcade.enable(this.tux);
	        		this.tux.scale.setTo(0.25, 0.25);
	        		this.shootingTux = true;
            }
        }
    },
    destroyTux: function(){
    	_this.shootingTux = false;
    	_this.tux.destroy();
    }
};