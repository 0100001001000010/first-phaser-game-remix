/*global game_state loadText game Phaser debug*/
var i =[];                                                                      //Index values for use in for loops.
var _this;
game_state.main = function() {};
game_state.main.prototype = {
    
    preload: function() {
        loadText = game.add.text(16, 16, "Loading...", {font: '16px monospace', fill: '#0f0'});
        game.load.spritesheet('gate', 'assets/img/gate.png', 64, 600);
        game.load.spritesheet('platformBlock', 'assets/img/hexdigits.png', 10, 14);
        game.load.image('meltdown', 'assets/img/meltdownlogo.png');
        game.load.image('smallMS', 'assets/img/smallms.png');
        game.load.spritesheet('player', 'assets/img/stickman.png', 26, 76);
        game.load.spritesheet('tux', 'assets/img/tux.png', 100, 100);
        game.load.spritesheet('virus', 'assets/img/virus.png', 37, 37);
    },
    
    create: function() {
		try {
			game.world.setBounds(0, 0, 24000, 1200);

			//Creating the ledges
			console.log("Generating the ledges...");
			this.ledges     = game.add.group();
			this.ledgesLeft = game.add.group();
			this.ledges.add(this.ledgesLeft);
			for (i[0] = 0; i[0] < 1200; i[0] += 14){
				this.ledgesLeft.create(0, i[0], 'platformBlock');
				this.ledgesLeft.getAt(this.ledgesLeft.length - 1).frame = Math.floor(Math.random() * 16);
			}
			
			this.ledgesTop  = game.add.group();
			this.ledges.add(this.ledgesTop);
			for (i[0] = 0; i[0] < 24000; i[0] += 10){
				this.ledgesTop.create(i[0], 0, 'platformBlock');
				this.ledgesTop.getAt(this.ledgesTop.length - 1).frame = Math.floor(Math.random() * 16);
			}
			
			this.ledgeRight = game.add.sprite(23936, 0, 'gate');
			this.ledgeRight.scale.setTo(1, 2);
			this.ledges.add(this.ledgeRight);
			
			//Start placing the platforms
			console.log("Generating the level...");
			this.platforms = game.add.group();
			//Variables to temproarily store platform location information.
			var locationTempX = 10;
			var locationTempY = 1190;
			var locationYrange;
			var locationXdelta;
			var timeoutID = window.setTimeout(alert, 30000, "The level generation appears to be taking a while. If you browser asks you, do not stop any scripts or the page.");
			while (locationTempX < 24000){                                      //Generate platforms primarily by X coordinate.
				if (Math.round(Math.random())) {                                //If true, extend the current platform. Otherwise, make a new one.
					locationTempX += 10;
				} else {
					locationXdelta = Math.ceil(Math.random() * 5);
					locationTempX += locationXdelta;
 					locationYrange = Math.round(Math.sqrt(10000 - Math.pow(locationXdelta, 2)) / 10);
					locationTempY += Math.round(Math.random()) ?
						10 * Math.ceil(Math.random()*locationYrange) :
						-10 * Math.ceil(Math.random()*locationYrange);
					if(locationTempY < 98) {                                    //Validate the generated values.
						locationTempY=98;
					} else if (locationTempY > 1190) {
						locationTempY = 1190;
					} else if (locationTempX >= 23990) {
						break;
					}
				}
				this.platforms.create(locationTempX, locationTempY, 'platformBlock');
				this.platforms.getAt(this.platforms.length - 1).frame = Math.floor(Math.random() * 16);
			}
			window.clearTimeout(timeoutID);

			//Placing the objects
			this.enemies    = game.add.group();
			this.microsofts = game.add.group();
			this.viruses    = game.add.group();
			this.enemies.add(this.microsofts);
			this.enemies.add(this.viruses);
			console.log("Creating the enemies...");
			for (i[0] = 0; i[0] < 24000; i[0] += 10){
				if (!(Math.floor(Math.random() * 50))) {
					var enemyType = Math.floor(Math.random() * 10) ?
						'virus' :
						'smallMS' ;
					this.enemies.create(i[0], 14, enemyType);
					if (enemyType == 'virus'){
						this.viruses.add(this.enemies.getAt(this.enemies.length - 1));
						this.viruses.getAt(this.viruses.length - 1).animations.add('spin', [0,1], 10, true);
						this.viruses.getAt(this.viruses.length - 1).animations.play('spin');
					} else {
						this.microsofts.add(this.enemies.getAt(this.enemies.length - 1));
					}
				}
			}
			i[0] = Math.floor(Math.random() * this.platforms.length);
			this.meltdown = game.add.sprite(1000 + Math.random() * 2000, 14, 'meltdown');
			this.meltdown.scale.setTo(0.1,0.1);
			this.player = game.add.sprite(this.platforms.getAt(0).x, 14, 'player');
			this.player.animations.add ('left', [0, 1, 2, 3, 4], 10, true);
	        this.player.animations.add ('right',[6, 7, 8, 9, 10],10, true);
			this.player.frame = 5;
			
			//Set up the physics and controls.
			game.physics.startSystem(Phaser.Physics.ARCADE);
			game.physics.arcade.enable(this.ledges);
			game.physics.arcade.enable(this.platforms);
			game.physics.arcade.enable(this.enemies);
			game.physics.arcade.enable(this.player);
			game.physics.arcade.enable(this.meltdown);
			this.enemies.enableBody = true;
			this.player.enableBody = true;
			this.ledges.enableBody = true;
			this.platforms.enableBody = true;
			this.meltdown.enableBody = true;
			this.platforms.forEach(function(item){
				item.body.immovable = true;
			});
			this.ledgesLeft.forEach(function(item){
				item.body.immovable = true;
			});
			this.ledgesTop.forEach(function(item){
				item.body.immovable = true;
			});
			this.ledgeRight.body.immovable = true;
			this.cursors = game.input.keyboard.createCursorKeys();
			this.player.body.gravity.y = 980;
			this.player.body.bounce.y = 0.3;
			this.viruses.forEach(function(item){
				item.body.gravity.y = 980;
				item.body.bounce.y = 0.3;
				item.health = 2;
				item.body.collideWorldBounds = true;
			});
			this.microsofts.forEach(function(item){
				item.body.gravity.y = 980;
				item.body.bounce.y = 0.3;
				item.health = 1;
				item.body.collideWorldBounds = true;
			});
			this.meltdown.body.gravity.y = 980;
			this.keys = {
				WASD: {
					up:    game.input.keyboard.addKey(Phaser.Keyboard.W),
					left:  game.input.keyboard.addKey(Phaser.Keyboard.A),
					down:  game.input.keyboard.addKey(Phaser.Keyboard.S),
					right: game.input.keyboard.addKey(Phaser.Keyboard.D)
				}
			};
				
			loadText.text = "";
			loadText.fixedToCamera = true;
			game.camera.follow(this.player);                                         //Set up the camera to follow the player
			this.player.health = 5;
			this.shootingTux = false;
			this.hasMeltdown = false;
	        _this = this;
			console.log("Game started!");
		} catch (err) {
			alert(err);
		}
    },
    
    update: function() {
    	try {
    		loadText.text = "Health: " + _this.player.health;
    		if (this.player.y > 1200){
    			this.player.damage(1);
    		} if (!this.player.alive){
    			game.state.start('crashed-Microsoft');
    		}
    		game.physics.arcade.collide(this.player, this.ledgesTop);
    	    game.physics.arcade.collide(this.player, this.ledgesLeft);
        	game.physics.arcade.collide(this.player, this.ledgeRight, function(){
        		if (_this.hasMeltdown){
        			_this.meltdown = game.add.sprite(192, 314, 'meltdown');
        			game.state.start('story_beforeBosslevel');
        		} else {
        			alert("I can't get into kernel space! You need to use Meltdown");
        		}
        	});
	        game.physics.arcade.collide(this.player, this.platforms);
	        game.physics.arcade.collide(this.player, this.meltdown, function(){
	        	_this.hasMeltdown = true;
	        	_this.meltdown.kill();
	        });
	        
	        game.physics.arcade.collide(this.enemies, this.ledgesTop);
	        game.physics.arcade.collide(this.enemise, this.ledgesLeft);
	        game.physics.arcade.collide(this.enemies, this.ledgeRight);
	        game.physics.arcade.collide(this.enemies, this.platforms);
	        game.physics.arcade.collide(this.enemies, this.meltdown);
	        
	        game.physics.arcade.collide(this.meltdown, this.ledgesTop);
	        game.physics.arcade.collide(this.meltdown, this.ledgesLeft);
	        game.physics.arcade.collide(this.meltdown, this.ledgeRight);
	        game.physics.arcade.collide(this.meltdown, this.platforms);
    		
    		//Manage the player
	        if (this.cursors.left.isDown){
    	        this.player.body.velocity.x = -500;
        	    this.player.animations.play('left');
	        } else if (this.cursors.right.isDown){
    	        this.player.body.velocity.x = 500;
        	    this.player.animations.play('right');
	        } else {
    	        this.player.body.velocity.x = 0;
        	    this.player.animations.stop();
            	this.player.frame = 5;
	        }
    	    if (this.cursors.up.isDown && this.player.body.touching.down) {
        	    this.player.body.velocity.y = -500;
	        }
	        
	        //Viruses
	        this.viruses.forEach(function(item){
	        	if (item.x < _this.player.x){                                   //If the virus is to the left of the player
	        		item.body.velocity.x = 250;
	        	} if (item.x > _this.player.x) {                                //If it is to the right of the player
	        		item.body.velocity.x = -250;
	        	} if (item.body.touching.down){                                 //If touching something, jump
	        		item.body.velocity.y = -500;
	        	}
	        	game.physics.arcade.collide(item, _this.tux, function(){
	        		_this.destroyTux();
	        		item.damage(1);
	        	});
	        	game.physics.arcade.collide(item, _this.player, function(){
	        		_this.player.damage(1);
	        		item.kill();
	        	});
	        });
	        
	        //Microsofts
	        this.microsofts.forEach(function(item){
	        	if (item.x < _this.player.x){
	        		item.body.velocity.x = 50;
	        	} if (item.x > _this.player.x){
	        		item.body.velocity.x = -50;
	        	} if (item.body.touching.down){
	        		item.body.velocity.y = -1000;
	        	}
	        	game.physics.arcade.collide(item, _this.tux, function(){
	        		_this.destroyTux();
	        		item.damage(1);
	        	});
	        	game.physics.arcade.collide(item, _this.player, function(){
	        		game.state.start('crashed-Microsoft');
	        	});
	        });
	        
	        //Firing the bullet
	        if (this.shootingTux){
	        	if (this.tux.frame === 0) {
	        		this.tux.body.velocity.y = -1000;
	        	} else if (this.tux.frame == 1) {
	        		this.tux.body.velocity.x = 1000;
	        	} else if (this.tux.frame == 2) {
	        		this.tux.body.velocity.y = 1000;
	        	} else {
	        		this.tux.body.velocity.x = -1000;
	        	}
	        } else {
	        	if (this.keys.WASD.right.isDown) {
	        		this.tux = game.add.sprite(this.player.x, this.player.y, 'tux');
	        		this.tux.frame = 1;
	        		this.tux.enableBody = true;
	        		game.physics.arcade.enable(this.tux);
	        		this.tux.scale.setTo(0.25, 0.25);
	        		this.shootingTux = true;
	        		this.destroyTuxTimer = game.time.create(true);
	        		this.destroyTuxTimer.add(1000, this.destroyTux, this);
	        		this.destroyTuxTimer.start();
	        	} else if (this.keys.WASD.left.isDown) {
	        		this.tux = game.add.sprite(this.player.x, this.player.y, 'tux');
	        		this.tux.frame = 3;
	        		this.tux.enableBody = true;
	        		game.physics.arcade.enable(this.tux);
	        		this.tux.scale.setTo(0.25, 0.25);
	        		this.shootingTux = true;
	        		this.destroyTuxTimer = game.time.create(true);
	        		this.destroyTuxTimer.add(1000, this.destroyTux, this);
	        		this.destroyTuxTimer.start();
	        	} else if (this.keys.WASD.up.isDown) {
	        		this.tux = game.add.sprite(this.player.x, this.player.y, 'tux');
	        		this.tux.frame = 0;
	        		this.tux.enableBody = true;
	        		game.physics.arcade.enable(this.tux);
	        		this.tux.scale.setTo(0.25, 0.25);
	        		this.shootingTux = true;
	        		this.destroyTuxTimer = game.time.create(true);
	        		this.destroyTuxTimer.add(1000, this.destroyTux, this);
	        		this.destroyTuxTimer.start();
	        	} else if (this.keys.WASD.down.isDown) {
	        		this.tux = game.add.sprite(this.player.x, this.player.y, 'tux');
	        		this.tux.frame = 2;
	        		this.tux.enableBody = true;
	        		game.physics.arcade.enable(this.tux);
	        		this.tux.scale.setTo(0.25, 0.25);
	        		this.shootingTux = true;
	        		this.destroyTuxTimer = game.time.create(true);
	        		this.destroyTuxTimer.add(1000, this.destroyTux, this);
	        		this.destroyTuxTimer.start();
	        	}
	        }
    	} catch(err) {
    		alert(err);
    	}
    },
    destroyTux: function(){
    	_this.shootingTux = false;
    	_this.tux.destroy();
    }
};
