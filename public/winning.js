/*global game game_state dialogTextLeft, dialogTextRight*/
game_state.winning = function(){}
game_state.winning.prototype = {
    preload: function(){
        game.load.spritesheet('player', 'assets/img/stickman.png', 26, 76);
        game.load.spritesheet('MSCEO', 'assets/img/satyanadella.png', 400, 452);
    },
    
    create: function(){
        dialogTextLeft = game.add.text(16, 16, "", {font: '16px monospace', fill:'#0f0'});
        dialogTextRight = game.add.text(416, 16, "", {font: '16px monospace', fill:'#0f0'});
        this.MSCEO = game.add.sprite(200, 113, "MSCEO");
        this.MSCEO.frame = 1;
        this.player = game.add.sprite(10, 510, 'player');
        this.player.frame = 5;
        this.dialogTimer = game.time.create(true);
        
        this.dialogTimer.add(0, function(){dialogTextRight.text = "You have defeated me!";}, this);
        this.dialogTimer.add(840, function(){
            dialogTextRight.text = "";
            dialogTextLeft.text = "Yes I have!";
        }, this);
        this.dialogTimer.add(1280, function(){
            dialogTextLeft.text = "";
            dialogTextRight.text = "Fine, I will leave the people alone.";
        }, this);
        this.dialogTimer.add(2720, function(){dialogTextRight.text = "Oh, look at the time!";}, this);
        this.dialogTimer.add(3560, function(){dialogTextRight.text = "I've got to force people in the real\nworld to install the Windows 10\nCompletely-Useless-But-Takes-Eighteen-\nHours-To-Install Update!";}, this);
        this.dialogTimer.add(9640, function(){
            dialogTextRight.text = "";
            dialogTextLeft.text = "Necessary attribution is in the credits.txt file. View the source code at\nhttps://github.com/0100001001000010/first-phaser-game-remix.\nThank you for playing!";
        }, this);
        this.dialogTimer.start();
    },
    
    update: function(){
        
    }
};