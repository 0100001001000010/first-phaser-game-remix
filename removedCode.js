        //Setting up the player
        this.player = game.add.sprite (32, game.world.height - 150, 'dude');
        game.physics.arcade.enable(this.player);
        this.player.body.bounce.y = 0.9;
        this.player.body.gravity.y = 980;
        this.player.body.bounce.x = 0.5;
        this.player.body.collideWorldBounds = true;
        //The two animations
        this.player.animations.add('left', [0, 1, 2, 3], 10, true);
        this.player.animations.add('right',[5, 6, 7, 8], 10, true);
        
        this.stars = game.add.group();                                          //The stars
        this.stars.enableBody = true;
        for (var i = 0; i < 100; i++) {
            var star = this.stars.create(20+Math.random()*760, Math.random() * 300, 'star');
            star.body.gravity.y = 300;
            star.body.bounce.y = 0.5 + Math.random() * 0.5;
        }
        
        this.scoreText = game.add.text (16, 16, 'Collect stars to score points!', {fontSize: '32px', fill: '#000'});
        this.score = 0;

game.physics.arcade.collide(this.player, this.platforms);
game.physics.arcade.overlap(this.player, this.stars, this.collectStar, null, this);